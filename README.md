# Hello Kernel

This project was created as a compilation of information from different sources how to
get started with loadable Linux kernel modules.

## References

The Linux Kernel Module Programming Guide: https://tldp.org/LDP/lkmpg/2.6/html/x181.html<br>
How to sign things for Secure Boot : https://ubuntu.com/blog/how-to-sign-things-for-secure-boot

## Install tools

```
sudo apt-get install linux-headers-$(uname -r) build-essential
```

## Build and install
Build is simple (assuming that all development tools are avalilable): `make`

Installation should be done as a superuser: `sudo cp hello-1.ko /lib/modules/$(uname -r)/`

## Load module

`sudo modprobe hello-1`

If there are no errors, then use `dmesg | tail -20` to check if module prints message to the log and `sudo rmmod hello-1` to unload the module. Also you can stop reading here.

If you see error message like "modprobe: ERROR: could not insert 'hello_1': Operation not permitted", then you need to check kernellogs using `dmesg | tail -20`. "Lockdown: modprobe: unsigned module loading is restricted; see man kernel_lockdown.7" promts you to sign module and then kernel will accept it.

## How to deal with the Secure Boot
Double check if Secure Boot is enabled: `sudo mokutil --sb-state`

Create key:
```
cd self-signing
openssl req -config ./openssl.cnf -new -x509 -newkey rsa:4096 -nodes -days 36500 -outform DER -keyout "MOK.priv" -out "MOK.der"
```

Enroll key: `sudo mokutil --import MOK.der`. Then you need to reboot your device and confirm key import.

Sign module: `kmodsign sha512 self-signing/MOK.priv self-signing/MOK.der hello-1.ko`

Double check the result: `hexdump -Cv hello-1.ko | tail -n 5`<br>You should see "Module signature appended" at the end of output.

```
$ sudo cp hello-1.ko /lib/modules/$(uname -r)/hello-1.ko
$ sudo modprobe hello-1
$ sudo rmmod hello_1
$ dmesg | tail -5
[  373.117062] hello_1: loading out-of-tree module taints kernel.
[  373.117474] Hello world 1.
[  405.410219] Goodbye world 1.
```